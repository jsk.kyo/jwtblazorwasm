﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class SD
    {
        public const string Role_Admin = "Admin";
        public const string Role_Customer = "Customer";
        public const string Role_Employee = "Employee";

        //인증/로그인 관련 
        public const string Local_Token = "JWT Token";

        ////로컬 스토리지 로그인 사용자 정보 저장
        public const string Local_UserDetails = "User Details";

        //public const string Local_InitialBooking = "InitialRoomBookingInfo";
        //public const string Local_RoomOrderDetails = "RoomOrderDetails";

        
        ////인증/로그인 관련 

        //public const string Status_Pending = "Pending";
        //public const string Status_Booked = "Booked";
        //public const string Status_CheckedIn = "CheckedIn";
        //public const string Status_CheckedOut_Completed = "CheckedOut";
        //public const string Status_NoShow = "NoShow";
        //public const string Status_Cancelled = "Cancelled";
    }
}
