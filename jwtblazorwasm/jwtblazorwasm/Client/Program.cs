using Blazored.LocalStorage;
using jwtblazorwasm.Client.Service;
using jwtblazorwasm.Client.Service.IService;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace jwtblazorwasm.Client
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            //api URI 가져오기 appsettings.json 에서
            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.Configuration.GetValue<string>("BaseAPIUrl")) });
            //builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });


            //wsam블레이저 로컬스토리지 사용
            builder.Services.AddBlazoredLocalStorage();

            //JWT 토큰 DI Inject
            builder.Services.AddAuthorizationCore();
            builder.Services.AddScoped<AuthenticationStateProvider, AuthStateProvider>();
            //인증 서비스 의존성 주입
            builder.Services.AddScoped<IAuthenticationService, AuthenticationService>();


            await builder.Build().RunAsync();
        }
    }
}
